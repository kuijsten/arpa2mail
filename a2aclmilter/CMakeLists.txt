#
# CMAKE SETUP (version, build style)
#
# SPDX-License-Identifier: ISC
# SPDX-FileCopyrightText: 2019 Tim Kuijsten
cmake_minimum_required (VERSION 3.13 FATAL_ERROR)
project ("ARPA2 milter" LANGUAGES C)
set(CMAKE_C_STANDARD 11)

include (FeatureSummary)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake)

### OPTIONS / BUILD SETTINGS
#
#
option (NO_TESTING "Disable testing" OFF)
if (NOT NO_TESTING)
	enable_testing()
endif()

# In debug configurations, add compile flag.
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG")
add_definitions(-Wall -Wextra -pedantic)

### DEPENDENCIES
#
#
find_library(A2ID_LIBRARY NAMES arpa2id PATHS "/usr/local/lib")
find_library(A2ACLC_LIBRARY NAMES arpa2aclc PATHS "/usr/local/lib")
find_library(MILTER_LIBRARY NAMES milter PATHS "/usr/local/lib")

if (A2ID_LIBRARY)
	message(STATUS "${A2ID_LIBRARY}")
else()
	message(FATAL_ERROR "ARPA2 ID library is required.")
endif()

if (A2ACLC_LIBRARY)
	message(STATUS "${A2ACLC_LIBRARY}")
else()
	message(FATAL_ERROR "ARPA2 ACL library is required.")
endif()

if (MILTER_LIBRARY)
	message(STATUS "${MILTER_LIBRARY}")
else()
	message(FATAL_ERROR "Sendmail milter library is required.")
endif()



### EXECUTABLE
#
#

add_executable(a2aclmilter a2aclmilter.c util.c)

target_include_directories(a2aclmilter PRIVATE /usr/local/include)
target_link_libraries(a2aclmilter "${A2ID_LIBRARY}" "${A2ACLC_LIBRARY}" "${MILTER_LIBRARY}")

# starting from CMake 3.14, drop DESTINATION and use platform defaults
install(TARGETS a2aclmilter DESTINATION sbin)
# starting from CMake 3.14, use TYPE MAN... instead of DESTINATION
install(FILES a2aclmilter.8 DESTINATION share/man/man8)


### PACKAGING
#
set (CPACK_PACKAGE_NAME "ARPA2-Milter")
set (CPACK_PACKAGE_VERSION ${ARPA2-Milter_VERSION})
set (CPACK_PACKAGE_VENDOR "ARPA2.net")
set (CPACK_PACKAGE_CONTACT "Tim Kuijsten")
# License information for packaging. This uses the SPDX license
# identifiers from https://spdx.org/licenses/
set (CPACK_PACKAGE_LICENSE "ISC")

include (CPack)

feature_summary(
	WHAT ALL
	FATAL_ON_MISSING_REQUIRED_PACKAGES
	QUIET_ON_EMPTY
)
